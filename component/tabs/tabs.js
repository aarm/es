import React, {useState} from 'react';
import Head from "next/head";


const Tabs =  () => {

    const [tabs, setTabs] = useState(['All', 'Formula','NBA', 'Boxing', 'UFC', 'Sport', 'WWE'])
    const [selected, setSelected] = useState(0)

    const handleTabs = (index) => {
        if (index == 0) {
            setSelected(0)
        } else {
            [tabs[index], tabs[1]] = [tabs[1], tabs[index]]
            setTabs([...tabs])
            setSelected(1)
        }
    }

    return <>
        <div className='d-flex mb-2 overflow-auto'>
            {
                tabs?.map((item, index) => {
                    return <div role='button' onClick={() => handleTabs(index)} style={{color: selected == index ? 'white' : 'black', background: selected == index ? 'red' : 'white', borderRadius: '5px', marginLeft: '8px', padding: '8px 20px 8px 20px'}} >{item}</div>
                })
            }
        </div>
    </>
}

export default Tabs;