import React from 'react';
import style from './header.module.css'
import {AiFillLinkedin, AiFillFacebook, AiFillTwitterSquare} from 'react-icons/ai'

const Header = () => {

    return <>
      <div className={style?.header}>
          <div className='d-md-block d-none'><h5 role='button'>Home</h5></div>
          <div><img role='button' style={{height: '34px'}} loading="lazy" src={'https://image-cdn.essentiallysports.com/wp-content/uploads/es_horizontal-1.png'}/></div>
          <div className='d-md-block d-none'>
              <AiFillLinkedin className={style?.socialIcon}/>
              <AiFillFacebook className={style?.socialIcon}/>
              <AiFillTwitterSquare className={style?.socialIcon}/>
          </div>
      </div>
    </>
}

export default Header;