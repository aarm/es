import React from 'react';
import Head from "next/head";
import style from "./topHeadline.module.css";
import { useQuery } from "react-query";
import {getHeadline} from '../../services/services'

const TopHeadLine =  () => {

    const fav = [{img: 'https://image-cdn.essentiallysports.com/wp-content/uploads/haiiday-sharp-473x315.jpg', name: '“I Wasn’t Supposed to Live”: 33-Year-Old Equestrian Aims to Overcome Dark Past\n' +
            'by Looking to Achieve an Incredible Feat'},
        {img: 'https://image-cdn.essentiallysports.com/wp-content/uploads/GettyImages-1252548960-473x315.jpg', name: '8 Years After Their Rollercoaster Final, Novak Djokovic Finally Spills the Beans\n' +
                'on Facing Roger Federer – ‘That Match Was a…’'},
        {img: 'https://image-cdn.essentiallysports.com/wp-content/uploads/GettyImages-1169999909-450x315.jpg', name: 'Embarrassed MVP Shaquille O’Neal Gave Celtics Legend a Nickname the NBA\n' +
                'Community Could Never Forget: “I Didn’t Know He Was That Good”'},
        {img: 'https://image-cdn.essentiallysports.com/wp-content/uploads/imago1027509797h-473x315.jpg',name: 'Agony at Augusta: Tiger Woods Breaks Millions of Hearts With Gut-Wrenching\n' +
                'Announcement at the Masters' },
        {img: 'https://image-cdn.essentiallysports.com/wp-content/uploads/nba-1200x628-imago-final-17-2-560x293.png', name: '“Once He Get His Money This Man Is Not Gonna Play”: Fans Thrash Kyrie Irving\n' +
                'After Billionaire Owner Mark Cuban Gets Acute Advice for Luka Doncic and the\n' +
                'Mavs'},]


    /**
     * Function to fetch the headline news
     */
    async function fetchTopHeadlineNews(){
        return getHeadline()
    }

    // const {data, isLoading } = useQuery('headline', fetchTopHeadlineNews)
    return <>
        <Head>
            <title>Create Next App</title>
        </Head>
        <div className={'mt-4 ' + style?.fav_home}>
            <h6 className='border-bottom p-2'>Top Headlines</h6>

            {
                fav.map((item, index) => {
                    return  <div className='d-flex align-items-center border-bottom mt-2 p-2'>
                        <img className={style?.fav_icon} loading="lazy" src={item?.img}/>
                        <div className={style?.fav_details}>{item?.name}</div>
                    </div>
                })
            }
        </div>
    </>
}

export default TopHeadLine;