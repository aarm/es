import React from 'react';
import Head from "next/head";
import style from './favorite.module.css'


const Favorite =  () => {

    const fav = [{img: 'https://a.espncdn.com/combiner/i?img=%2Fi%2Fleaguelogos%2Fcricket%2F500%2F8048.png&w=60&h=60&scale=crop&cquality=80&location=origin', name: 'IPL'},
        {img: 'https://a4.espncdn.com/combiner/i?img=%2Fi%2Fleaguelogos%2Fsoccer%2F500%2F23.png&w=60&h=60&scale=crop&cquality=80&location=origin', name: 'English Premier League'},
        {img: 'https://a.espncdn.com/combiner/i?img=%2Fi%2Fleaguelogos%2Fsoccer%2F500%2F15.png&w=60&h=60&scale=crop&cquality=80&location=origin', name: 'La Liga'},
        {img: 'https://a1.espncdn.com/combiner/i?img=%2Fi%2Fleaguelogos%2Fsoccer%2F500%2F10.png&w=60&h=60&scale=crop&cquality=80&location=origin',name: 'Bundesliga' },
        {img: 'https://a1.espncdn.com/combiner/i?img=%2Fredesign%2Fassets%2Fimg%2Ficons%2FESPN%2Dicon%2Dmma.png&w=80&h=80&scale=crop&cquality=40&location=origin', name: 'Ultimate Fighting Championship'},
        {img: 'https://a.espncdn.com/combiner/i?img=%2Fredesign%2Fassets%2Fimg%2Ficons%2FESPN%2Dicon%2Dsoccer.png&w=80&h=80&scale=crop&cquality=40&location=origin', name: 'I-League'}]

    return <>
        <Head>
            <title>Create Next App</title>
        </Head>
        <div className={'mt-4 ' + style?.fav_home}>
            <h6 className='border-bottom p-2'>Your Favorite</h6>

            {
                fav.map((item, index) => {
                    return  <div className='d-flex align-items-center border-bottom mt-2 p-2'>
                        <img className={style?.fav_icon} loading="lazy" src={item?.img}/>
                        <div className={style?.fav_details}>{item?.name}</div>
                    </div>
                })
            }
        </div>
    </>
}

export default Favorite;