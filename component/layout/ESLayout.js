import React from 'react';

const ESLayout = ({children}) => {

    return <div style={{background: '#EDEEF0', height: '100%'}}>
        {children}
    </div>
}

export default ESLayout;