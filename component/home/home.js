import React from 'react';
import Head from "next/head";
import Favorite from "../favorites/favorite";
import TopHeadLine from "../topHeadline/topHeadline";
import News from "../news/news";


const Homes =  () => {

    return <>
        <Head>
            <title>Create Next App</title>
        </Head>
        <div  className='container-fluid'>
            <div className='row'>
                <div className='col-md-3 d-md-block d-none'>
                    <Favorite/>
                </div>
                <div className='col-md-6 col-xs-12 col-sm-12'>
                    <News/>
                </div>
                <div className='col-md-3 d-md-block d-none'>
                    <TopHeadLine/>
                </div>
            </div>
        </div>
    </>
}

export default Homes;