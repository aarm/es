import React from 'react';
import Tabs from "../tabs/tabs";
import {getNews} from "../../services/services";
import {useQuery} from "react-query";
import style from './news.module.css'
import {Skeleton} from "antd";

const News = () => {

    /**
     * Function to fetch the news
     */
    async function fetchNews(){
        return getNews()
    }

    const {data, isLoading } = useQuery('headline', fetchNews)

    return <>
       <div className='tabs mt-4'>
           <Tabs/>
       </div>
        <div style={{height: '90vh', overflow: 'auto'}}>

            {
                isLoading ? Array(5).fill(0).map(() => {
                    return <Skeleton active />
                }) : null
            }

            {
                data?.map((item, index) => {
                    return <div className={'mt-1 mb-2 '+style?.card} onClick={() => window.open(item?.link, '_blank')}>
                        <img style={{borderRadius: '15px 15px 0px 0px'}} className={'w-100 ' + style?.hoverImg} src={item?.enclosure?.link}/>
                        <div className='p-3'>
                            <h6 className={style.title}>{item?.title}</h6>
                            <div style={{fontSize: '12px'}}>{item?.pubDate}</div>
                        </div>
                    </div>
                })
            }
        </div>
    </>
}

export default News;