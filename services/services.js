import axios from "axios";


export async function getHeadline(token) {
    const url = `https://www.essentiallysports.com/api/editors-posts/`;
    let response = await axios.get(url, { headers: {referrer: 'https://www.essentiallysports.com/category/nba/?utm_source=website&utm_medium=internal_links&utm_campaign=category_bar'}});
    return response.data
}

export async function getNews(token) {
    const url = `https://api.rss2json.com/v1/api.json?rss_url=https://www.essentiallysports.com/feed/`;
    let response = await axios.get(url, { });
    return response.data?.items
}