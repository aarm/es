import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Header from "../component/header/header";
import Homes from "../component/home/home";
import ESLayout from "../component/layout/ESLayout";
import {
    QueryClient,
    QueryClientProvider
} from "react-query";
import { ReactQueryDevtools } from 'react-query/devtools'


export default function Home() {

    const queryClient = new QueryClient();
    if (typeof window !== 'undefined') {
        const h1 = document?.getElementById('checkdv_1');
        h1?.remove();
    }

  return (
    <div>
        <QueryClientProvider client={queryClient} contextSharing={true}>
      <Head>
        <title>ES</title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp"
              crossOrigin="anonymous"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N"
                crossOrigin="anonymous"></script>

          <link rel="preconnect" href="https://fonts.googleapis.com"/>
              <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin/>
                  <link
                      href="https://fonts.googleapis.com/css2?family=Archivo:ital,wght@1,500&family=Nunito:wght@500;900&display=swap"
                      rel="stylesheet"/>

      </Head>
      <main>
        <div>
            <Header/>
            <ESLayout>
                <Homes/>
            </ESLayout>
        </div>
      </main>
            <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
    </div>
  )
}
